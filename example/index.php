<?php
$config = array(
        "authorize_endpoint" =>
         "http://127.0.0.1/OAuth2-Server/index.php/oauth2/authorize",
        "client_id" => "client-id",//change
        "client_secret" => "client-secret",//change
        "token_endpoint" => "http://localhost/OAuth2-Server/index.php/oauth2/token",
        );
$otherConfig=array(
		"test"=>"this is test a nother config",
);
require_once 'vendor/autoload.php';
//$request = $_GET['to'];
//if (array_key_exists($request, $config)) {
    $clientConfig = new OAuth\Client\ClientConfig($config,$otherConfig);
    $tokenStorage = new OAuth\Client\SessionStorage();
    $httpClient = new \Guzzle\Http\Client();
    $api = new OAuth\Client\Api("foo", $clientConfig, $tokenStorage, $httpClient);
    $context = new OAuth\Client\Context("john.doe@example.org", new OAuth\Client\Scope("publish_feed,photo_upload"));

    $accessToken = $api->getAccessToken($context);
	//print_r($api->getAuthorizeUri($context));
	//exit;
    if (false === $accessToken) {
        /* no valid access token available, go to authorization server */
        header("HTTP/1.1 302 Found");
        header("Location: " . $api->getAuthorizeUri($context));
        exit;
    }

    try {
        $client = new \Guzzle\Http\Client();
        $bearerAuth = new \fkooman\Guzzle\Plugin\BearerAuth\BearerAuth($accessToken->
            getAccessToken());
        $client->addSubscriber($bearerAuth);
        $response = $client->get($apiUri)->send();
        //header("Content-Type: application/json");
        $value = html_entity_decode($response->getBody());
        $val = json_decode($value);
        print_r($val->user_id);
    }
    catch (\fkooman\Guzzle\Plugin\BearerAuth\Exception\BearerErrorResponseException
        $e) {
        if ("invalid_token" === $e->getBearerReason()) {
            // the token we used was invalid, possibly revoked, we throw it away
            $api->deleteAccessToken($context);
            $api->deleteRefreshToken($context);
            /* no valid access token available, go to authorization server */
            header("HTTP/1.1 302 Found");
            header("Location: " . $api->getAuthorizeUri($context));
            exit;
        }
        throw $e;
    }
    catch (\Exception $e) {
        die(sprintf('ERROR: %s', $e->getMessage()));
    }
//} else {
  //  echo "Error : this app not allow this api from" . $request;
//}
